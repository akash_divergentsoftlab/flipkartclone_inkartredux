import "./productContainer.styles.scss";
import React from "react";
import CategoryProduct from "./category_product";

export default function CategoryProductContainer() {
  return (
    <>
      <hr />

      <h3>Showing the Products of category_name:</h3>
      <hr />
      <ul class="auto-grid">
        <CategoryProduct />
      </ul>
    </>
  );
}
