import "./productContainer.styles.scss";
import React from "react";
import ShopProduct from "./ShopProduct";

function ShopProductContainer() {
  return (
    <>
      <br />
      <ul class="auto-grid">
        <ShopProduct />
      </ul>
    </>
  );
}

export default ShopProductContainer;
