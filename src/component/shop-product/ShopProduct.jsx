import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { URL_ADDRESS } from "../../Default";
import "./productContainer.styles.scss";

function ShopProduct() {
  const [products, setProducts] = useState([]);

  const getProducts = () =>
    fetch(`${URL_ADDRESS}/product`).then((res) => res.json());

  useEffect(() => {
    getProducts().then((products) => setProducts(products));
  }, []);

  return (
    <>
      {products.map((product) => (
        <li
          class="product"
          style={{
            padding: "0rem -1rem",
            textAlign: "center",
            fontSize: "14px",
            background: "#ffffff",
            color: "#ffffff",
          }}
        >
          <Link class="img-wrapper" to={`/product-detail/${product.productId}`}>
            <img
              src={product.imageUrl}
              alt={product.productName}
              style={{ height: "206px", width: "247px" }}
            />
          </Link>
          <div class="info">
            <div class="title">
              <Link to={`/product-detail/${product.productId}`}>
                {product.productName.slice(0, 25)}
              </Link>
            </div>
            <div class="price">₹{product.price}</div>
          </div>
          <div class="actions-wrapper">
            <a href="/#" class="add-btn wishlist">
              {" "}
              Wishlist{" "}
            </a>
            <a href="/#"  onClick={() =>
              dispatch(
                allActions.cartActions.addToCart(
                  productId,
                  productName,
                  price,
                  imageUrl
                )
              )
            }
            class="add-btn cart">
              {" "}
              Cart{" "}
            </a>
          </div>
        </li>
      ))}
    </>
  );
}

export default ShopProduct;
