import React from "react";
import { Link } from "react-router-dom";

function MainTopNavBar() {
  return (
    <div>
      <nav
        className="navbar navbar-expand-lg navbar-light bg-dark navbar-
            fixed-top"
      >
        <a
          className="navbar-brand"
          href="/#"
          style={{
            color: "white",
            marginLeft: "160px",
            marginTop: "-14px",
            marginRight: "10px",
          }}
        >
          <b>
            <i>Inkart</i>
          </b>
        </a>

        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <input
            className="form-control col-sm-5"
            type="search"
            placeholder="Search for products,brand and more"
          />
          <i
            className="fa fa-search"
            style={{
              fontSize: "20px",
              color: "royalblue",
              marginLeft: "485px",
              position: "fixed",
            }}
          ></i>
          <p
            style={{ color: "white", marginBottom: "1px", marginLeft: "190px" }}
          >
            <b>Login & Signup</b>
          </p>
          <Link className="_3ko_Ud" to="/shop/" style={{ marginLeft: "35px" }}>
            <span
              style={{ color: "whitesmoke", fontSize: "18", marginLeft: "5px" }}
            >
              <b>Shop</b>
            </span>
          </Link>
          <li className="nav-item dropdown" style={{ listStyle: "none" }}>
            <a
              className="nav-link dropdown-toggle"
              id="navbarDropdown"
              role="button"
              href="/#"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
              style={{ color: "white", marginTop: "1px", marginLeft: "25px" }}
            >
              <b> More</b>
            </a>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
              <a aria-hidden="true" href="/#" style={{ marginLeft: "1px" }}>
                My Account info
              </a>
              <div className="dropdown-divider"></div>
              <a aria-hidden="true" href="/#" style={{ marginLeft: "1px" }}>
                Account Settings
              </a>
              <div className="dropdown-divider"></div>
              <a aria-hidden="true" href="/#">
                Logout
              </a>
            </div>
          </li>
          <img
            alt=""
            src="https://cdn5.vectorstock.com/i/1000x1000/77/24/white-shopping-cart-icon-isolated-on-transparent-vector-25547724.jpg"
            style={{ height: "19px", backgroundColor: "white" }}
          />
          <a
            className="_3ko_Ud"
            href="/viewcart?otracker=Cart_Icon_Click"
            style={{ marginLeft: "35px" }}
          >
            <svg
              className="_2fcmoV"
              width="14"
              height="14"
              viewBox="0 0 16
                        16"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                className="_2JpNOH"
                d="M15.32 2.405H4.887C3 2.405
                            2.46.805 2.46.805L2.257.21C2.208.085 2.083 0 1.946
                            0H.336C.1 0-.064.24.024.46l.644 1.945L3.11
                            9.767c.047.137.175.23.32.23h8.418l-.493
                            1.958H3.768l.002.003c-.017 0-.033-.003-.05-.003-1.06
                            0-1.92.86-1.92 1.92s.86 1.92 1.92 1.92c.99 0
                            1.805-.75 1.91-1.712l5.55.076c.12.922.91 1.636 1.867
                            1.636 1.04 0 1.885-.844 1.885-1.885
                            0-.866-.584-1.593-1.38-1.814l2.423-8.832c.12-.433-.206-.86-.655-.86"
                fill="#fff"
              ></path>
            </svg>
            <span
              style={{ color: "whitesmoke", fontSize: "18", marginLeft: "5px" }}
            >
              <b>Cart</b>
            </span>
          </a>
        </div>
      </nav>
    </div>
  );
}

export default MainTopNavBar;
