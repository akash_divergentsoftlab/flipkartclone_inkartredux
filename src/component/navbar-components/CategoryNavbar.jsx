import React, { useEffect, useState } from "react";
import { URL_ADDRESS } from "../../Default";
import { Link } from "react-router-dom";

function CategoryNavbar() {
  const [Electonics, setElectonics] = useState([]);
  const [MensFashions, setMensFashion] = useState([]);
  const [WomensFashions, setWomensFashion] = useState([]);
  const [kitchens, setkitchen] = useState([]);
  const [grocerys, setgrocery] = useState([]);
  const [books, setbooks] = useState([]);
  const [audios, setaudio] = useState([]);
  const [sportsandfitnesss, setsportsandfitness] = useState([]);
  const [allcomputerandaccessoriess, setallcomputerandaccessories] = useState(
    []
  );

  const getElectonics = () =>
    fetch(`${URL_ADDRESS}/sub_category/electronics`).then((res) => res.json());

  const getMensFashions = () =>
    fetch(`${URL_ADDRESS}/sub_category/men's fashion`).then((res) =>
      res.json()
    );

  const getWomensFashions = () =>
    fetch(`${URL_ADDRESS}/sub_category/women's fashion`).then((res) =>
      res.json()
    );

  const getkitchens = () =>
    fetch(`${URL_ADDRESS}/sub_category/kitchen`).then((res) => res.json());

  const getgrocerys = () =>
    fetch(`${URL_ADDRESS}/sub_category/grocery`).then((res) => res.json());

  const getbooks = () =>
    fetch(`${URL_ADDRESS}/sub_category/books`).then((res) => res.json());

  const getaudios = () =>
    fetch(`${URL_ADDRESS}/sub_category/audio`).then((res) => res.json());

  const getsportsandfitnesss = () =>
    fetch(`${URL_ADDRESS}/sub_category/sports and fitness`).then((res) =>
      res.json()
    );

  const getallcomputerandaccessoriess = () =>
    fetch(`${URL_ADDRESS}/sub_category/all computer & accessories`).then(
      (res) => res.json()
    );

  useEffect(() => {
    getElectonics().then((Electonics) => setElectonics(Electonics));
    getMensFashions().then((MensFashions) => setMensFashion(MensFashions));
    getWomensFashions().then((WomensFashions) =>
      setWomensFashion(WomensFashions)
    );
    getkitchens().then((kitchens) => setkitchen(kitchens));
    getgrocerys().then((grocerys) => setgrocery(grocerys));
    getbooks().then((books) => setbooks(books));
    getaudios().then((audios) => setaudio(audios));
    getsportsandfitnesss().then((sportsandfitnesss) =>
      setsportsandfitness(sportsandfitnesss)
    );
    getallcomputerandaccessoriess().then((allcomputerandaccessoriess) =>
      setallcomputerandaccessories(allcomputerandaccessoriess)
    );
  }, []);

  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Electronics
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {Electonics.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Men's Fashions
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {MensFashions.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Women's Fashions
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {WomensFashions.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Kitchens
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {kitchens.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Grocerys
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {grocerys.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Books
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {books.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Audios
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {audios.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                Sports And fitnesss
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {sportsandfitnesss.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
          <ul className="navbar-nav">
            <li className="nav-item dropdown" style={{ listStyle: "none" }}>
              <a
                className="nav-link dropdown-toggle"
                href="/#"
                id="navbarDropdownMenuLink"
                role="button"
                data-toggle="dropdown"
                style={{
                  color: "black",
                  marginLeft: "30px",
                  fontSize: "15px",
                }}
              >
                All computer and accessoriess
              </a>
              <div
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink"
              >
                {allcomputerandaccessoriess.map((item) => (
                  <Link
                    className="dropdown-item"
                    key={item.subCategoryId}
                    to={`/category/${item.subCategoryName}`}
                  >
                    {item.subCategoryName}
                  </Link>
                ))}
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default CategoryNavbar;
