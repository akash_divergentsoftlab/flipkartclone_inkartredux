import React, { useEffect, useState } from "react";
import { URL_ADDRESS } from "../../Default";

function CategoryNavbar() {
  const [categories, setCategories] = useState([]);
  const [subcategories, setSubCategories] = useState([]);

  const getCategory = () =>
    fetch(`${URL_ADDRESS}/category`).then((res) => res.json());

  const getSubCategory = () =>
    fetch(`${URL_ADDRESS}/sub_category`).then((res) => res.json());

  useEffect(() => {
    getCategory().then((categories) => setCategories(categories));
    getSubCategory().then((subcategories) => setSubCategories(subcategories));
  }, []);
  // console.log(categories);
  // console.log(subcategories);

  // groupby function 
  const cats = subcategories.reduce(
    (catMemo, { categoryName, subCategoryName }) => {
      (catMemo[categoryName] = catMemo[categoryName] || []).push(
        subCategoryName
      );
      return catMemo;
    },
    {}
  );
  console.log(cats);
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavDropdown"
          aria-controls="navbarNavDropdown"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavDropdown">
          {categories.map((category) => (
            <ul key={category.categoryId} className="navbar-nav">
              <li className="nav-item dropdown" style={{ listStyle: "none" }}>
                <a
                  className="nav-link dropdown-toggle"
                  href="/#"
                  id="navbarDropdownMenuLink"
                  role="button"
                  data-toggle="dropdown"
                  style={{
                    color: "black",
                    marginLeft: "30px",
                    fontSize: "15px",
                  }}
                >
                  {category.categoryName}
                </a>
                <div
                  className="dropdown-menu"
                  aria-labelledby="navbarDropdownMenuLink"
                >
                  <a className="dropdown-item" href="/#">
                    asd
                  </a>
                </div>
              </li>
            </ul>
          ))}
        </div>
      </nav>
    </div>
  );
}

export default CategoryNavbar;
