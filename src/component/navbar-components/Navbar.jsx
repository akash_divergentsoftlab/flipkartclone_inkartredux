import React from "react";
import CategoryNavbar from "./CategoryNavbar";
import MainTopNavBar from "./mainTopNavbar";

function Navbar() {
  return (
    <div>
      <MainTopNavBar />
      <br />
      <br />
      <CategoryNavbar/>
    </div>
  );
}

export default Navbar;
