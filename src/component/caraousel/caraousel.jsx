import React from "react";

function Caraousel() {
  return (
    <div>
      <div
        id="carouselExampleControls"
        className="carousel slide"
        data-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active" style={{ height: "380px" }}>
            <img
              src="https://scontent.fbho1-2.fna.fbcdn.net/v/t1.6435-9/67271070_2017188605053780_5745043697892851712_n.jpg?_nc_cat=108&ccb=1-5&_nc_sid=973b4a&_nc_ohc=r77Y5pXm-okAX91z3Nn&_nc_ht=scontent.fbho1-2.fna&oh=470efc58e3954285e96db1fe6cc4b344&oe=6143E120"
              className="d-block w-100"
              alt="..."
            />
          </div>
          <div className="carousel-item" style={{ height: "380px" }}>
            <img
              src="https://in.bmscdn.com/nmcms/events/banner/desktop/media-desktop-realme-x2-pro-launch-event-2019-11-8-t-13-49-20.jpg"
              className="d-block w-100"
              alt="..."
            />
          </div>
          <div className="carousel-item" style={{ height: "380px" }}>
            <img
              src="https://www.parentune.com/images/Banner-A.jpg"
              className="d-block w-100"
              alt="..."
            />
          </div>
          <div className="carousel-item" style={{ height: "380px" }}>
            <img
              src="https://www.xda-developers.com/files/2018/03/9-Lite-Face-Unlock-Banner-1280x440-pixel.jpg"
              className="d-block w-100"
              alt="..."
            />
          </div>
          <div className="carousel-item" style={{ height: "380px" }}>
            <img
              src="http://cdn1.lenskart.com/media/wysiwyg/blanco_wat/watchk_14jan_slider_01.jpg"
              className="d-block w-100"
              alt="..."
            />
          </div>
        </div>
        <a
          className="carousel-control-prev"
          href="#carouselExampleControls"
          role="button"
          data-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Previous</span>
        </a>
        <a
          className="carousel-control-next"
          href="#carouselExampleControls"
          role="button"
          data-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="sr-only">Next</span>
        </a>
      </div>
    </div>
  );
}

export default Caraousel;
