import "./App.css";
import { Switch, Route } from "react-router-dom";
import HomePage from "./pages/homepage/HomePage";
import Navbar from "./component/navbar-components/Navbar";
import CategoryPage from "./pages/category/CategoryPage";
import ShopPage from "./pages/shopPage/ShopPage";
import ProductDetailPage from "./pages/productDetail/ProductDetailPage";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/shop" component={ShopPage} />
        <Route exact path="/product-detail/:productId" component={ProductDetailPage} />
        <Route
          exact
          path="/category/:subcategoryName"
          component={CategoryPage}
        />
      </Switch>
    </div>
  );
}

export default App;
