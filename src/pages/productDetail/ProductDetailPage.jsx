import React from "react";
import ProductDetail from "../../component/productDetail/productDetail";
import "./ProductDetailPage.styles.scss";

function ProductDetailPage() {
  return (
    <div className="product-detail-page">
      <ProductDetail />
    </div>
  );
}

export default ProductDetailPage;
