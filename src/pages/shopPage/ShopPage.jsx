import React from "react";
import ShopProductContainer from "../../component/shop-product/ShopProductContainer";

function ShopPage() { 
  return (
    <div>
      <ShopProductContainer />
    </div>
  );
}

export default ShopPage;
