import React from "react";
import CategoryProductContainer from "../../component/category-products/product-container";

function CategoryPage() {
  
  return (
    <div>
      <CategoryProductContainer />
    </div>
  );
}

export default CategoryPage;
